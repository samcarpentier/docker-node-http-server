# docker-node-http-server

A simple apline-based [docker image](https://hub.docker.com/r/samcarpentier/node-http-server/) with Node http-server that can be used to serve static web pages.
